<?php


require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

use App\Covoiturage\Controleur\ControleurUtilisateur;
$controleur = $_GET['controleur'] ?? 'utilisateur';
$nomDeClasseControleur = 'App\Covoiturage\Controleur\Controleur' . ucfirst($controleur);
// On récupère l'action passée dans l'URL
$action = $_GET['action'] ?? 'afficherListe';
if(class_exists($nomDeClasseControleur)) {
    $methodesDispo = get_class_methods($nomDeClasseControleur);
    if (in_array($action, $methodesDispo)) {
        $nomDeClasseControleur::$action();
    } else {
        ControleurUtilisateur::afficherErreur("l'action  $action n'existe pas");
    }
}else {
    ControleurUtilisateur::afficherErreur("le controleur  $controleur n'existe pas");
}

?>
