<?php
if (isset($trajet)):
    ?>
    <h1>Détails du Trajet</h1>
    <p><strong>Départ :</strong> <?= htmlspecialchars($trajet->getDepart()) ?></p>
    <p><strong>Arrivée :</strong> <?= htmlspecialchars($trajet->getArrivee()) ?></p>
    <p><strong>Date :</strong> <?= htmlspecialchars($trajet->getDate()->format("d/m/Y")) ?></p>
    <p><strong>Prix :</strong> <?= htmlspecialchars($trajet->getPrix()) ?> €</p>
    <p><strong>Conducteur :</strong> <?= htmlspecialchars($trajet->getConducteur()->getPrenom() . ' ' . $trajet->getConducteur()->getNom()) ?></p>
    <p><strong>Non fumeur :</strong> <?= $trajet->isNonFumeur() ? 'Oui' : 'Non' ?></p>

    <h2>Passagers :</h2>
    <ul>
        <?php if (count($trajet->getPassagers()) > 0): ?>
            <?php foreach ($trajet->getPassagers() as $passager): ?>
                <li><?= htmlspecialchars($passager->getPrenom() . ' ' . $passager->getNom()) ?></li>
            <?php endforeach; ?>
        <?php else: ?>
            <li>Aucun passager pour ce trajet.</li>
        <?php endif; ?>
    </ul>

    <a href="liste.php?controleur=trajet">Retour à la liste des trajets</a>

<?php else: ?>
    <p>Aucune information disponible pour ce trajet.</p>
<?php endif; ?>
