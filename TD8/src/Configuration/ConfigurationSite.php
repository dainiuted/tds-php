<?php
namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    private static int $dureeExpirationSession = 1800; // 30 minutes

    public static function getDureeExpirationSession(): int
    {
        return self::$dureeExpirationSession;
    }
    public static function getURLAbsolue(): string
    {
        return "http://localhost/tds-php/TD8/web/controleurFrontal.php";
    }
    //returns true in development mode, false in production
    public static function getDebug():bool{
        return false;
    }
}

