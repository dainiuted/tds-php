<?php

use App\Covoiturage\Configuration\ConfigurationSite;
use App\Covoiturage\Modele\DataObject\Utilisateur;

/** @var Utilisateur $utilisateur */
?>
<form method="<?php echo ConfigurationSite::getDebug() ? 'GET' : 'POST'; ?>" action="controleurFrontal.php">
    <input type="hidden" name="action" value="mettreAJour"/>
    <input type="hidden" name="controleur" value="utilisateur">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="login" id="login_id"
                   value="<?= htmlspecialchars($utilisateur->getLogin()) ?>" readonly>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Leblanc" name="nom" id="nom_id"
                   value="<?= htmlspecialchars($utilisateur->getNom()) ?>" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prénom&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Jveux" name="prenom" id="prenom_id"
                   value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdpa_id">Mot de passe ancienne &#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdpa" id="mdpa_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="email_id">Email&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : a.b@yahoo.fr" name="email" id="email_id"
                   value="<?= htmlspecialchars($utilisateur->getEmail()) ?>" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
            <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin"
                   id="estAdmin_id" <?= htmlspecialchars($utilisateur->getEstAdmin() ? 'checked' : '') ?> >
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>