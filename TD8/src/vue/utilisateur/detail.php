<?php
use App\Covoiturage\Lib\ConnexionUtilisateur;
if (isset($utilisateur)): ?>
    <p>Le <strong>login</strong> de l'utilisateur <?= htmlspecialchars($utilisateur->getNom()) ?> <?= htmlspecialchars($utilisateur->getPrenom()) ?> est <?= htmlspecialchars($utilisateur->getLogin()) ?>
        <?php
        if (ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())): ?>
            (<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=<?= htmlspecialchars($utilisateur->getLogin()) ?>">Modifier</a>,
            <a href="controleurFrontal.php?controleur=utilisateur&action=supprimer&login=<?= htmlspecialchars($utilisateur->getLogin()) ?>">Supprimer</a>)
        <?php endif; ?>
    </p>
<?php endif; ?>
