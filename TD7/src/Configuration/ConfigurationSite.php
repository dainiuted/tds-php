<?php
namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    private static int $dureeExpirationSession = 1800; // 30 minutes

    public static function getDureeExpirationSession(): int
    {
        return self::$dureeExpirationSession;
    }
}

