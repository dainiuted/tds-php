<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php /**
         * @var string $titre
         */
        echo $titre; ?></title>
    <link rel="stylesheet" href="../ressources/css/navstyle.css">
</head>
<body>
<header>
    <nav>
        <!-- Votre menu de navigation ici -->
        <ul>
            <li>
                <a href="controleurFrontal.php?controleur=utilisateur&action=afficherListe">Gestion des
                    utilisateurs</a>
            </li>
            <li>
                <a href="controleurFrontal.php?controleur=trajet&action=afficherListe">Gestion des trajets</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulairePreference">
                    <img src="../ressources/img/heart.png" alt="Heart Icon" style="width:20px;height:20px;">
                </a>
            </li>
            <!--<li>
                <a href="controleurFrontal.php?controleur=utilisateur&action=testSession">Tester la Session</a>
            </li>-->
        </ul>
    </nav>

</header>
<main>
    <?php
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Daniele Dainiute
    </p>
</footer>
</body>
</html>

