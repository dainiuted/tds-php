<?php
require_once "Utilisateur.php";
$utilisateurs = Utilisateur::recupererUtilisateurs();
foreach ($utilisateurs as $utilisateur) {
    echo "<p> " . $utilisateur->__toString() . "</p>";
    $trajetsCommePassager = $utilisateur->getTrajetsCommePassager();

    if (!empty($trajetsCommePassager)) {
        echo "<ul>";
        foreach ($trajetsCommePassager as $trajet) {
            echo "<li>" . $trajet->__toString() . "</li>";
        }
        echo "</ul>";
    } else {
        echo "<p>Aucun trajet en tant que passager.</p>";
    }
}
?>