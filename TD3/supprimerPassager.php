<?php
require_once 'Trajet.php';
require_once 'Utilisateur.php';
require_once 'ConnexionBaseDeDonnees.php';

$login = $_GET['login'] ?? null;
$trajetId = $_GET['trajet_id'] ?? null;

if (!$login || !$trajetId) {
    echo "Login or trajet ID missing.";
    exit;
}

$trajet = Trajet::recupererTrajetParId((int)$trajetId);

if (!$trajet) {
    echo "Trajet not found.";
    exit;
}

if ($trajet->supprimerPassager($login)) {
    echo "Utilisateur $login a été supprimée du trajet.";
} else {
    echo "Échec de la suppression de l'utilisateur $login du trajet.";
}
?>