<?php

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

class Trajet
{

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;
    private array $passagers; // Ajout de l'attribut passagers

    public function __construct(
        ?int              $id,
        string            $depart,
        string            $arrivee,
        DateTime          $date,
        int               $prix,
        Utilisateur $conducteur,
        bool              $nonFumeur,
        array             $passagers = [] // Valeur par défaut pour les passagers
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = $passagers; // Initialisation des passagers
    }

    // Méthode pour construire un Trajet depuis un tableau SQL
    public static function construireDepuisTableauSQL(array $trajetTableau): Trajet
    {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // Transformation de la date string en DateTime
            $trajetTableau["prix"],
            Utilisateur::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]), // Récupération de l'objet Utilisateur via le login
            (bool)$trajetTableau["nonFumeur"] // Conversion automatique de l'entier en booléen
        );

        // Récupérer les passagers pour ce trajet
        $trajet->setPassagers($trajet->recupererPassagers());

        return $trajet;
    }

    // Accesseurs pour les passagers
    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }

    // Méthode pour récupérer les passagers d'un trajet depuis la base de données

    /**
     * @return Utilisateur[]
     */
    private function recupererPassagers(): array
    {
        $pdo = ConnexionBaseDeDonnees::getPDO();
        $sql = "SELECT utilisateur.login, utilisateur.nom, utilisateur.prenom 
                FROM passager
                INNER JOIN utilisateur ON passager.passagerLogin = utilisateur.login
                WHERE passager.trajetId = :trajetId";

        $stmt = $pdo->prepare($sql);
        $stmt->execute([':trajetId' => $this->id]);

        $passagers = [];
        while ($row = $stmt->fetch()) {
            $passagers[] = new Utilisateur($row['login'], $row['nom'], $row['prenom']);
        }

        return $passagers;
    }

    // Autres méthodes de la classe...

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }


    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
        </p>";
    }

    // Méthode pour ajouter un trajet à la base de données
    public function ajouter(): bool
    {
        try {
            $pdo = ConnexionBaseDeDonnees::getPDO();
            $sql = "INSERT INTO trajet (depart, arrivee, date, prix, conducteurLogin, nonFumeur)
            VALUES (:depart, :arrivee, :date, :prix, :conducteurLogin, :nonFumeur)";
            $stmt = $pdo->prepare($sql);
            $dateString = $this->date->format("Y-m-d");
            $conducteurLogin = $this->conducteur->getLogin();
            $nonFumeur = $this->nonFumeur ? 1 : 0;
            $stmt->execute([
                ':depart' => $this->depart,
                ':arrivee' => $this->arrivee,
                ':date' => $dateString,
                ':prix' => $this->prix,
                ':conducteurLogin' => $conducteurLogin,
                ':nonFumeur' => $nonFumeur
            ]);
            $this->id = $pdo->lastInsertId();
            return $this->id !== null;
        } catch (PDOException $e) {
            echo "Erreur lors de l'ajout du trajet : " . $e->getMessage();
            return false;
        }

    }

    // Méthode pour récupérer tous les trajets

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach ($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public function supprimerPassager(string $passagerLogin): bool
    {
        if (!$this->id) {
            return false;
        }

        $pdo = ConnexionBaseDeDonnees::getPDO();
        $stmt = $pdo->prepare("SELECT COUNT(*) FROM trajet WHERE id = :id");
        $stmt->execute([':id' => $this->id]);

        if ($stmt->fetchColumn() == 0) {
            return false;
        }
        $stmt = $pdo->prepare("DELETE FROM passager WHERE passagerLogin = :passagerLogin AND trajetId = :trajetId");
        $stmt->execute([':passagerLogin' => $passagerLogin, ':trajetId' => $this->id]);
        return $stmt->rowCount() > 0;
    }


    public static function recupererTrajetParId(int $id): ?Trajet
    {
        $pdo = ConnexionBaseDeDonnees::getPDO();
        $sql = "SELECT * FROM trajet WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([':id' => $id]);

        $trajetFormatTableau = $stmt->fetch();
        if (!$trajetFormatTableau) {
            return null;
        }

        return Trajet::construireDepuisTableauSQL($trajetFormatTableau);
    }
}