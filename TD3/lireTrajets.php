<?php
require_once "Trajet.php";
$trajets = Trajet::recupererTrajets();
foreach ($trajets as $trajet) {
    echo $trajet;

    echo "<h3>Passagers :</h3>";
    $passagers = $trajet->getPassagers();

    if (count($passagers) === 0) {
        echo "<p>Aucun passager pour ce trajet.</p>";
    } else {
        echo "<ul>";
        foreach ($passagers as $passager) {
            echo "<li>{$passager->getPrenom()} {$passager->getNom()} ({$passager->getLogin()}) ";
            echo "<a href='supprimerPassager.php?login={$passager->getLogin()}&trajet_id={$trajet->getId()}'>Désinscrire</a>";
            echo "</li>";
        }
        echo "</ul>";
    }

    echo "<hr>";
}
?>