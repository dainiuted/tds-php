<?php
require_once 'Trajet.php';
if (!empty($_POST)) {
    //$id = isset($_POST['id']) ? $_POST['id'] : '';
    $depart = isset($_POST['depart']) ? $_POST['depart'] : '';
    $arrivee = isset($_POST['arrivee']) ? $_POST['arrivee'] : '';
    $dateString = isset($_POST['dateString']) ? $_POST['dateString'] : '';
    $prix = isset($_POST['prix']) ? $_POST['prix'] : '';
    $conducteurLogin = isset($_POST['conducteurLogin']) ? $_POST['conducteurLogin'] : '';
    $nonFumeur = isset($_POST['nonFumeur']) ? $_POST['nonFumeur'] : '';

    $date = new DateTime($dateString);
    $conducteur = Utilisateur::recupererUtilisateurParLogin($conducteurLogin);
    if (!$conducteur) {
        echo "<p>Il n'y a pas d'utilisateur avec le login : " . htmlspecialchars($conducteurLogin) . "</p>";
    } else {
        $trajet = new Trajet(null, $depart, $arrivee, $date, $prix, $conducteur, $nonFumeur);
        $trajet->ajouter();

        echo "<p>" . $trajet . "</p>";
    }
} else {
    echo "<p>Aucune donnée reçue.</p>";
}
?>

