<?php
require_once 'Utilisateur.php';
if (!empty($_POST)) {
    /*echo "<pre>";
    //var_dump($_POST);
    echo "</pre>";*/

    $login = isset($_POST['login']) ? $_POST['login'] : '';
    $nom = isset($_POST['nom']) ? $_POST['nom'] : '';
    $prenom = isset($_POST['prenom']) ? $_POST['prenom'] : '';

    $utilisateur = new Utilisateur($login, $nom, $prenom);
    $utilisateur->ajouter();

    echo "<p>" . $utilisateur . "</p>";
} else {
    echo "<p>Aucune donnée reçue.</p>";
}
?>
