<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<h1>Liste des utilisateurs</h1>
<ul>
<?php
/** @var ModeleUtilisateur[] $utilisateurs */
foreach ($utilisateurs as $utilisateur): ?>
    <li>
        <a href="/tds-php/TD4/Controleur/routeur.php?action=afficherDetail&login=<?= urlencode($utilisateur->getLogin()) ?>">
        <?= $utilisateur->getPrenom() . " " . $utilisateur->getNom() ?>
        </a>
        (login: <?= $utilisateur->getLogin() ?>)
    </li>
<?php endforeach; ?>
</ul>
<!-- Lien vers le formulaire de création d'utilisateur -->
<a href="/tds-php/TD4/Controleur/routeur.php?action=afficherFormulaireCreation">Créer un utilisateur</a>
</body>
</html>
