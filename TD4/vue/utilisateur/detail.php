<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Detail des utilisateurs</title>
</head>
<body>
<?php if (isset($utilisateur)): ?>
    <p><?= $utilisateur->getNom() ?> <?= $utilisateur->getPrenom() ?>, <strong>
            login:</strong> <?= $utilisateur->getLogin() ?></p>

    <!--<h2>Trajets comme passager</h2>
    <?php if ($utilisateur->getTrajetsCommePassager()): ?>
        <ul>
            <?php foreach ($utilisateur->getTrajetsCommePassager() as $trajet): ?>
                <li><?= htmlspecialchars($trajet->__toString()) ?></li>
            <?php endforeach; ?>
        </ul>
    <?php else: ?>
        <p>Il n'a a pas des trajets comme passager pour ce utilisateur.</p>
    <?php endif; ?>-->

<?php else: ?>
    <p>Pas d'information pour utilisateur.</p>
<?php endif; ?>
</body>
</html>
