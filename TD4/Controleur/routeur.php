<?php

use src\web\ControleurUtilisateur;

require_once 'ControleurUtilisateur.php';
// On récupère l'action passée dans l'URL
$action = $_GET['action']??'afficherListe';
// Appel de la méthode statique $action de ControleurUtilisateur
if (method_exists('src\web\ControleurUtilisateur', $action)) {
    ControleurUtilisateur::$action();
} else {
    ControleurUtilisateur::afficherListe();
}
?>
