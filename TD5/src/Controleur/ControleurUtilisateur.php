<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\ModeleUtilisateur;

class ControleurUtilisateur
{
// Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); // appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Liste des utilisateurs',
            'cheminCorpsVue' => 'utilisateur/liste.php',
            'utilisateurs' => $utilisateurs
        ]);
    }

    public static function afficherDetail(): void
    {
        if (!isset($_GET['login'])) {
            self::afficherVue('vueGenerale.php', [
                'titre' => 'Erreurs',
                'cheminCorpsVue' => 'utilisateur/erreur.php',
            ]);
            return;
        }

        $login = $_GET['login'];
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);

        if (!$utilisateur) {
            self::afficherVue('vueGenerale.php', [
                'titre' => 'Erreurs',
                'cheminCorpsVue' => 'utilisateur/erreur.php',
            ]);
            return;
        }
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Details des utilisateurs',
            'cheminCorpsVue' => 'utilisateur/detail.php',
            'utilisateur' => $utilisateur
        ]);
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Formulaire pour creation des utilisateurs',
            'cheminCorpsVue' => 'utilisateur/formulaireCreation.php'
        ]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . '/../vue/' . $cheminVue;
    }

    public static function creerDepuisFormulaire(): void
    {
        if (isset($_GET['login'], $_GET['nom'], $_GET['prenom'])) {
            $login = $_GET['login'];
            $nom = $_GET['nom'];
            $prenom = $_GET['prenom'];

            $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);

            try {
                $utilisateur->ajouter();
                $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
                self::afficherVue('vueGenerale.php', [
                    'titre' => 'Utilisateur créé avec succès',
                    'cheminCorpsVue' => 'utilisateur/utilisateurCree.php',
                    'utilisateurs' => $utilisateurs
                ]);
            } catch (\PDOException $e) {
                error_log($e->getMessage());

                self::afficherVue('vueGenerale.php', [
                    'titre' => 'Erreur d\'ajout d\'utilisateur',
                    'cheminCorpsVue' => 'utilisateur/erreur.php',
                    'message' => 'L\'utilisateur existe déjà. Veuillez utiliser un autre login.'
                ]);
            }
        } else {
            self::afficherVue('vueGenerale.php', [
                'titre' => 'Erreur',
                'cheminCorpsVue' => 'utilisateur/erreur.php',
                'message' => 'Tous les champs sont requis.'
            ]);
        }
    }


}

?>