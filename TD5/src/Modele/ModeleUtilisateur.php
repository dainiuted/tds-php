<?php
namespace App\Covoiturage\Modele;

class ModeleUtilisateur
{

    private string $login;
    private string $nom;
    private string $prenom;
    private ?array $trajetsCommePassager;

    // un getter
    public function getNom()
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setLogin(string $login)
    {
        $this->login = substr($login, 0, 64);
    }

    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager == null) {
            $this->trajetsCommePassager = $this->recupererTrajetsCommePassager();
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }


    // un constructeur
    public function __construct(
        $login,
        $nom,
        $prenom
    )
    {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    /*public function __toString(): string
    {
        return "ModeleUtilisateur : " . $this->prenom . " " . $this->nom . ", login : " . $this->login;
    }*/

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): ModeleUtilisateur
    {
        return new ModeleUtilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }

    public static function recupererUtilisateurs()
    {
        $utilisateurs = [];
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
        if ($pdoStatement) {
            foreach ($pdoStatement as $utilisateurFormatTableau) {
                $utilisateurs[] = ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
            }
        }
        return $utilisateurs;
    }

    public static function recupererUtilisateurParLogin(string $login): ?ModeleUtilisateur
    {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
        );

        $pdoStatement->execute($values);

        $utilisateurFormatTableau = $pdoStatement->fetch();
        if (!$utilisateurFormatTableau) {
            return null;
        }
        return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter(): bool
    {
        try {
            $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES(:login, :nom, :prenom)";
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

            $values = array(
                "login" => $this->login,
                "nom" => $this->nom,
                "prenom" => $this->prenom,
            );

            $pdoStatement->execute($values);
            return true;

        } catch (PDOException $e) {
            if ($e->getCode() === '23000') {
                echo "Erreur : Un utilisateur avec ce login existe déjà.";
            } elseif ($e->getCode() === '22001') {
                echo "Erreur : Les données fournies sont trop longues.";
            } else {
                echo "Erreur lors de l'ajout de l'utilisateur : " . $e->getMessage();
            }

            return false;
        }
    }


}

?>

