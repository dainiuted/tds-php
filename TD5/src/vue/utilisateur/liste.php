<h1>Liste des utilisateurs</h1>
<ul>
    <?php
    use App\Covoiturage\Modele\ModeleUtilisateur;

    /** @var ModeleUtilisateur[] $utilisateurs */


    foreach ($utilisateurs as $utilisateur):
        $loginHTML = htmlspecialchars($utilisateur->getLogin());
        $loginURL = rawurlencode($utilisateur->getLogin());
        ?>
        <li>
            <a href="controleurFrontal.php?action=afficherDetail&login=<?= $loginURL ?>">
                <?= htmlspecialchars($utilisateur->getPrenom()) . " " . htmlspecialchars($utilisateur->getNom()) ?>
            </a>
            (login: <?= $loginHTML ?>)
        </li>
    <?php endforeach; ?>
</ul>
<!-- Lien vers le formulaire de création d'utilisateur -->
<a href="controleurFrontal.php?action=afficherFormulaireCreation">Créer un utilisateur</a>

