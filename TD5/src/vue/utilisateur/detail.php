<?php if (isset($utilisateur)): ?>
    <p><?= htmlspecialchars($utilisateur->getNom()) ?> <?= htmlspecialchars($utilisateur->getPrenom()) ?>, <strong>
            login:</strong> <?= htmlspecialchars($utilisateur->getLogin()) ?></p>


<?php else: ?>
    <p>Pas d'information pour utilisateur.</p>
<?php endif; ?>
