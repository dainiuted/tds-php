<?php


require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');
use App\Covoiturage\Controleur\ControleurUtilisateur;

// On récupère l'action passée dans l'URL
$action = $_GET['action'] ?? 'afficherListe';
// Appel de la méthode statique $action de ControleurUtilisateur
if (method_exists('App\Covoiturage\Controleur\ControleurUtilisateur', $action)) {
    ControleurUtilisateur::$action();
} else {
    ControleurUtilisateur::afficherListe();
}
?>
