<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>

    <body>
        Voici le résultat du script PHP :
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */

          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          //$texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
         // echo $texte;
        $prenom = "Marc";

        /*echo "Bonjour\n " . $prenom;
        echo "Bonjour\n $prenom";
        echo 'Bonjour\n $prenom';

        echo $prenom;
        echo "$prenom";*/
        $nom = "Joan";
        $login = "joanm";
        echo "<p>ModeleUtilisateur $nom de login $login</p>";
        $utilisateur =[
            'prenom' => 'Juste',
            'nom'    => 'Leblanc',
            'login'  => 'leblancj'
        ];
        var_dump($utilisateur);
        echo "<p> Utilisateur " . $utilisateur['prenom'] . " " . $utilisateur['nom'] . " de login " . $utilisateur['login']  .  " </p>";

        $utilisateurs =[[
            'prenom' => 'Juste',
            'nom'    => 'Leblanc',
            'login'  => 'leblancj'
        ],
        [
            'prenom' => 'Durand',
            'nom'    => 'Jean',
            'login'  => 'jeand'
        ],
        [
            'prenom' => 'Martin',
            'nom'    => 'Lucas',
            'login'  => 'lucasm'
        ]];
        var_dump($utilisateurs);
        if(!empty($utilisateurs)){
            echo "<h2>Liste des utilisateurs :</h2>";
            echo "<ul>";
            foreach ($utilisateurs as $utilisateur) {
                echo "<li>" . $utilisateur['prenom'] . " " . $utilisateur['nom'] . " de login " . $utilisateur['login']  . "</li>";
            }
            echo "</ul>";

        }
        else echo "<p> Il n'y a aucun utilisateur. </p>";
        ?>
    </body>
</html> 