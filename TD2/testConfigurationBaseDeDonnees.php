<?php
// On inclut les fichiers de classe PHP pour pouvoir se servir de la classe ConfigurationBaseDeDonnees.
// require_once évite que ConfigurationBaseDeDonnees.php soit inclus plusieurs fois,
// et donc que la classe ConfigurationBaseDeDonnees soit déclaré plus d'une fois.
require_once 'ConfigurationBaseDeDonnees.php';

// On affiche le login de la base de donnees
echo "Login : " . ConfigurationBaseDeDonnees::getLogin(). "<br/>";
echo "Nom de l'hôte : " . ConfigurationBaseDeDonnees::getNomHote(). "<br/>";
echo "Port : " . ConfigurationBaseDeDonnees::getPort() . "<br/>";
echo "Nom de la base de données : " . ConfigurationBaseDeDonnees::getNomBaseDeDonnees(). "<br/>";
echo "Mot de passe : " . ConfigurationBaseDeDonnees::getPassword(). "<br/>";

?>


