<?php
require_once "ConnexionBaseDeDonnees.php";
class Utilisateur
{

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom()
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setLogin(string $login)
    {
        $this->login = substr($login, 0, 64);
    }


    // un constructeur
    public function __construct(
        $login,
        $nom,
        $prenom
    )
    {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString(): string
    {
        return "Utilisateur : " . $this->prenom . " " . $this->nom . ", login : " . $this->login;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): ModeleUtilisateur
    {
        return new Utilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }

    public static function recupererUtilisateurs()
    {
        $utilisateurs = [];
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
        if ($pdoStatement) {
            foreach ($pdoStatement as $utilisateurFormatTableau) {
                $utilisateurs[] = Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
            }
        }
        return $utilisateurs;
    }
}

?>

