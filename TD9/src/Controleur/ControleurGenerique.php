<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Lib\PreferenceControleur;
use JetBrains\PhpStorm\NoReturn;

class ControleurGenerique
{

    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherErreur(string $messageErreur = "", string $controleur = "utilisateur"): void
    {
        self::afficherVue('vueGenerale.php', ['messageErreur' => $messageErreur, 'controleur' => $controleur, 'titre' => "Erreur", 'cheminCorpsVue' => 'erreur.php']);
    }

    public static function afficherFormulairePreference(): void
    {

        ControleurGenerique::afficherVue('vueGenerale.php', ["titre" => "Formulaire Préférence", "cheminCorpsVue" => "formulairePreference.php"]);
    }

    public static function enregistrerPreference(): void
    {
        $preference = $_REQUEST['controleur_defaut'];
        PreferenceControleur::enregistrer($preference);
    }

    #[NoReturn] public static function redirectionVersURL(string $type, string $message, string $url): void
    {
        MessageFlash::ajouter($type,$message);
        header("Location: controleurFrontal.php?action=$url");
        exit();
    }

}