<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\HTTP\Cookie;
use Exception;
use Random\RandomException;

// chargement du modèle
class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
    }


    public static function afficherDetail(): void
    {
        $login = $_REQUEST["login"];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
        if (empty($login)) {
            MessageFlash::ajouter("danger", "Il n'y a aucun login");
            return;
        } else {
            if (empty($utilisateur)) {
                MessageFlash::ajouter('warning', 'Login inconnu');
                ControleurUtilisateur::afficherListe();
            } else {
                ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Détail de l'utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
            }
        }
    }


    public static function afficherFormulaireCreation(): void
    {
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Formulaire de création d'utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    /**
     * @throws RandomException
     */
    public static function creerDepuisFormulaire(): void
    {
        $mdp = $_REQUEST['mdp'] ?? '';
        $mdp2 = $_REQUEST['mdp2'] ?? '';

        if ($mdp !== $mdp2) {
            MessageFlash::ajouter("danger", "Les mots de passes ne correspondent pas");
            return;
        }
        $estAdminConnecte = ConnexionUtilisateur::estAdministrateur();
        $utilisateur = self::construireDepuisFormulaire($_REQUEST);

        if (!filter_var($utilisateur->getEmailAValider(), FILTER_VALIDATE_EMAIL)) {
            MessageFlash::ajouter("danger", "Email invalide");
            return;
        }

        if (!$estAdminConnecte) {
            $utilisateur->setEstAdmin(false);
        }

        (new UtilisateurRepository)->ajouter($utilisateur);

        VerificationEmail::envoiEmailValidation($utilisateur);
        $utilisateurs = (new UtilisateurRepository)->recuperer();

        MessageFlash::ajouter('success', 'Utilisateur crée !');
        ControleurUtilisateur::afficherListe();
    }


    public static function afficherErreurUtilisateur(string $messageErreur = " "): void
    {
        self::afficherErreur($messageErreur, 'utilisateur');
    }

    public static function supprimer(): void
    {
        $login = $_REQUEST["login"];

        if (empty($login)) {
            MessageFlash::ajouter("danger", "Cet utilisateur n'existe pas ");
            return;
        }
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if (empty($utilisateur)) {
            MessageFlash::ajouter("danger", "L'utilisateur avec ce login n'existe pas.");
            return;
        }
        $utilisateurConnecte = ConnexionUtilisateur::getLoginUtilisateurConnecte();
        $estAdmin = ConnexionUtilisateur::estAdministrateur();

        if (!$estAdmin) {
            if (!$login === ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())) {
                MessageFlash::ajouter("danger", "La suppression n’est possible que pour l’utilisateur connecté.");
                return;
            }
        }

        (new UtilisateurRepository())->supprimer($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        MessageFlash::ajouter('success', 'Le utilisateur a bien été supprimé !');
        ControleurUtilisateur::afficherListe();
    }


    public static function afficherFormulaireMiseAJour(): void
    {
        $login = $_REQUEST["login"];
        $utilisateurConnecte = ConnexionUtilisateur::getLoginUtilisateurConnecte();
        $estAdminConnecte = ConnexionUtilisateur::estAdministrateur();

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if (!$utilisateur) {
            $messageErreur = $estAdminConnecte ? "Login inconnu" : "La mise à jour n’est possible que pour l’utilisateur connecté";
            MessageFlash::ajouter("danger", $messageErreur);
            return;
        }

        if (!$estAdminConnecte && $utilisateurConnecte !== $login) {
            MessageFlash::ajouter("danger","La mise à jour n’est possible que pour l’utilisateur connecté" );
            return;
        }

        ControleurUtilisateur::afficherVue('vueGenerale.php', [
            "utilisateur" => $utilisateur,
            "titre" => "Mise à jour d'un utilisateur",
            "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"
        ]);
    }

    public static function mettreAJour(): void
    {
        if (empty($_REQUEST['login']) || empty($_REQUEST['nom']) || empty($_REQUEST['prenom']) || empty($_REQUEST['mdpa']) || empty($_REQUEST['mdp']) || empty($_REQUEST['mdp2'] || empty($_REQUEST['email']))) {
            MessageFlash::ajouter("danger", "Tous les champs obligatoires doivent être remplis.");
            //ControleurUtilisateur::afficherErreur("Tous les champs obligatoires doivent être remplis.");
            return;
        }

        $login = $_REQUEST['login'];
        $ancienMdp = $_REQUEST['mdpa'];
        $nouveauMdp = $_REQUEST['mdp'];
        $nouveauMdp2 = $_REQUEST['mdp2'];
        $email = $_REQUEST['email'];
        $estAdminConnecte = ConnexionUtilisateur::estAdministrateur();
        $utilisateurConnecte = ConnexionUtilisateur::getLoginUtilisateurConnecte();

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if (!$utilisateur) {
            MessageFlash::ajouter("danger", "L'utilisateur avec ce login n'existe pas.");
            return;
        }

        if ($nouveauMdp !== $nouveauMdp2) {
            MessageFlash::ajouter("danger","Mots de passe distincts" );
            return;
        }

        if (!$estAdminConnecte && !MotDePasse::verifier($ancienMdp, $utilisateur->getMdpHache())) {
            MessageFlash::ajouter("danger","L'ancien mot de passe est incorrect." );
            return;
        }

        if (!$estAdminConnecte && $utilisateurConnecte !== $login) {
            MessageFlash::ajouter("danger","La mise à jour n’est possible que pour l’utilisateur connecté" );
            return;
        }

        $utilisateur->setNom($_REQUEST['nom']);
        $utilisateur->setPrenom($_REQUEST['prenom']);

        if (!empty($nouveauMdp)) {
            $mdpHache = MotDePasse::hacher($nouveauMdp);
            $utilisateur->setMdpHache($mdpHache);
        }

        if ($email !== $utilisateur->getEmail()) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                MessageFlash::ajouter("danger", "Email invalide");
                return;
            }
            $nonce = MotDePasse::genererChaineAleatoire();
            $utilisateur->setNonce($nonce);
            $utilisateur->setEmailAValider($email);
            VerificationEmail::envoiEmailValidation($utilisateur);
        }


        if ($estAdminConnecte) {
            $estAdmin = isset($_REQUEST['estAdmin']) && (bool)$_REQUEST['estAdmin'];
            $utilisateur->setEstAdmin($estAdmin);
        }

        (new UtilisateurRepository())->mettreAJour($utilisateur);
        MessageFlash::ajouter("success", "L'utilisateur de login {$utilisateur->getId()} a bien été mis à jour");

        self::afficherVue('vueGenerale.php', [
            "login" => $utilisateur->getLogin(),
            "titre" => "Mise à jour d'utilisateur effectuée",
            "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"
        ]);

        /*$mdpHache = MotDePasse::hacher($nouveauMdp);
        $estAdmin = isset($_REQUEST["estAdmin"]) && (bool)$_REQUEST["estAdmin"];
        $utilisateur = new Utilisateur($login, $_REQUEST["nom"], $_REQUEST["prenom"], $mdpHache, $estAdmin);
        (new UtilisateurRepository)->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "login" => $utilisateur->getLogin(), "titre" => "Mise à jour  d'utilisateur effectuée", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);*/
    }


    /**
     * @param array $tableauDonneesFormulaire
     * @return Utilisateur
     * @throws RandomException
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        /*foreach ($_REQUEST as $key => $value) {
            $tab[] = $value;
        }
        $tab[5] = MotDePasse::hacher($tab[5]);
        $utilisateur = new Utilisateur($tab[2], $tab[3], $tab[4], $tab[5], isset($_REQUEST["estAdmin"]));
        return $utilisateur;*/

        $mdpHache = MotDePasse::hacher($tableauDonneesFormulaire['mdp']);

        $emailAValider = $tableauDonneesFormulaire['email'];

        $nonce = MotDePasse::genererChaineAleatoire();

        $utilisateur = new Utilisateur(
            $tableauDonneesFormulaire['login'],
            $tableauDonneesFormulaire['nom'],
            $tableauDonneesFormulaire['prenom'],
            $mdpHache,
            false,
            "",
            $emailAValider,
            $nonce
        );

        return $utilisateur;
    }

    public static function validerEmail(): void
    {
        $login = $_REQUEST['login'] ?? '';
        $nonce = $_REQUEST['nonce'] ?? '';

        if (empty($login) || empty($nonce)) {
            MessageFlash::ajouter("danger", "Veuillez remplir tous les champs");
            return;
        }

        if (!VerificationEmail::traiterEmailValidation($login, $nonce)) {
            MessageFlash::ajouter("danger", "Traitement email n'as pas valide.");
            return;
        }

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        MessageFlash::ajouter("success","Votre adresse email a été validée avec succès." );
        self::afficherVue('vueGenerale.php', [
            "titre" => "Validation réussie",
            "utilisateur" => $utilisateur,
            "cheminCorpsVue" => "utilisateur/detail.php"
        ]);
    }


    public static function afficherFormulaireConnexion(): void
    {
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Formulaire de connection d'utilisateur", "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"]);
    }

    public static function connecter(): void
    {
        $login = $_REQUEST["login"];
        $mdpL = $_REQUEST["mdpL"];
        if (empty($login) || empty($mdpL)) {
            MessageFlash::ajouter("warning", "Veuillez remplir tous les champs");
            return;
        }
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if (empty($utilisateur)) {
            MessageFlash::ajouter("danger","Login incorrect" );
            return;
        }

        if (!MotDePasse::verifier($mdpL, $utilisateur->getMdpHache())) {
            MessageFlash::ajouter("danger", "Mot de passe incorrect");
            return;
        }
        if (!VerificationEmail::aValideEmail($utilisateur)) {
            MessageFlash::ajouter("danger", "Email pas validé");
            return;
        }

        ConnexionUtilisateur::connecter($utilisateur->getLogin());
        MessageFlash::ajouter('success', 'Utilisateur connecté');
        ControleurUtilisateur::afficherVue('vueGenerale.php', [
            "utilisateur" => $utilisateur,
            "titre" => "Utilisateur connecté",
            "cheminCorpsVue" => "utilisateur/detail.php"
        ]);

    }

    public static function deconnecter(): void
    {
        ConnexionUtilisateur::deconnecter();
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        MessageFlash::ajouter("success", "Utilisateur déconnecté" );

        ControleurUtilisateur::afficherVue('vueGenerale.php', [
            "utilisateurs" => $utilisateurs,
            "titre" => "Utilisateur déconnecté",
            "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php"
        ]);
    }

    public static function testerSession(): void
    {
        try {
            // Démarrer la session
            $session = Session::getInstance();
            echo "Session démarrée avec succès.<br>";

            // 1. Enregistrer une chaîne de caractères
            $session->enregistrer("utilisateur", "Cathy Penneflamme");
            echo "Utilisateur enregistré : " . $session->lire("utilisateur") . "<br>";

            // 2. Enregistrer un tableau
            $tableau = ["role" => "admin", "email" => "cathy@example.com"];
            $session->enregistrer("profil", $tableau);
            echo "Profil enregistré : ";
            print_r($session->lire("profil"));
            echo "<br>";

            // 3. Enregistrer un objet
            $utilisateur = new \stdClass();
            $utilisateur->nom = "Penneflamme";
            $utilisateur->prenom = "Cathy";
            $utilisateur->email = "cathy@example.com";
            $session->enregistrer("objetUtilisateur", $utilisateur);
            echo "Objet utilisateur enregistré : ";
            print_r($session->lire("objetUtilisateur"));
            echo "<br>";

            // 4. Supprimer une variable de session
            $session->supprimer("profil");
            echo "Profil supprimé.<br>";

            // Vérifier si la variable a été supprimée
            if (!$session->contient("profil")) {
                echo "La variable 'profil' n'existe plus dans la session.<br>";
            }

            // 5. Détruire complètement la session
            $session->detruire();
            echo "Session détruite et cookie de session supprimé.<br>";
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }
    /*public static function deposerCookie(){

         Cookie::enregistrer($_REQUEST["cle"],$_REQUEST["valeur"],$_REQUEST["dureeExpiration"]);
    }

    public static function lireCookie($cle){
        return Cookie::lire($cle);
    }*/


}

