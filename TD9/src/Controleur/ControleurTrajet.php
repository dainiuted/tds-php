<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;
use Exception;

class ControleurTrajet extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        ControleurTrajet::afficherVue('vueGenerale.php', ["trajets" => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/liste.php"]);
    }


    public static function afficherDetail(): void
    {
        $id = $_REQUEST["id"];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id); //appel au modèle pour gérer la BD
        if (empty($id)) {

            MessageFlash::ajouter("danger", "Il n'y a aucun id de trajet");
            return;
        } else {
            if (empty($trajet)) {
                MessageFlash::ajouter("danger", "Le trajet  n'existe pas");
                return;
            } else {
                ControleurTrajet::afficherVue('vueGenerale.php', ["trajet" => $trajet, "titre" => "Détail du trajet ", "cheminCorpsVue" => "trajet/detail.php"]);
            }
        }


    }


    public static function afficherFormulaireCreation(): void
    {
        ControleurTrajet::afficherVue('vueGenerale.php', ["titre" => "Formulaire de création de trajet ", "cheminCorpsVue" => "trajet/formulaireCreation.php"]);
    }

    /**
     * @throws Exception
     */
    public static function creerDepuisFormulaire(): void
    {


        $trajet = self::construireDepuisFormulaire($_REQUEST);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        MessageFlash::ajouter("success", "Le trajet a bien été créé !");

        ControleurTrajet::afficherVue('vueGenerale.php', ["trajet" => $trajet, "trajets" => $trajets, "titre" => "Trajet créé", "cheminCorpsVue" => "trajet/trajetCree.php"]);;


    }

    public static function afficherErreurTrajet(string $messageErreur = " "): void
    {
        self::afficherErreur($messageErreur, 'trajet');
    }

    public static function supprimer(): void
    {

        $id = $_REQUEST["id"];


        if (empty($id)) {
            MessageFlash::ajouter("error", "Ce trajet n'existe pas ");
            return;
        } else {

            (new TrajetRepository())->supprimer($id);
            $trajets = (new TrajetRepository())->recuperer();
            MessageFlash::ajouter("success", "Le trajet de id {$id} a bien été supprimé");
            ControleurTrajet::afficherVue("vueGenerale.php", ["titre" => "Trajet supprime", "trajets" => $trajets, "cheminCorpsVue" => "trajet/trajetSupprime.php"]);

        }

    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $id = $_REQUEST["id"];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        ControleurTrajet::afficherVue('vueGenerale.php', ["trajet" => $trajet, "titre" => "Mise à jour d'un trajet", "cheminCorpsVue" => "trajet/formulaireMiseAJour.php"]);

    }

    /**
     * @throws Exception
     */
    public static function mettreAJour(): void
    {
        $trajet = self::construireDepuisFormulaire($_REQUEST);
        (new TrajetRepository)->mettreAJour($trajet);
        MessageFlash::ajouter("success", "Le trajet {$trajet->getId()} a bien été mis à jour");
        $trajets = (new TrajetRepository)->recuperer();
        self::afficherVue('vueGenerale.php', ["trajets" => $trajets, "id" => $trajet->getId(), "titre" => "Mise à jour du trajet effectuée", "cheminCorpsVue" => "trajet/trajetMisAJour.php"]);
    }

    /**
     * @param array $tableauDonneesFormulaire
     * @return Trajet
     * @throws Exception
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        foreach ($_REQUEST as $key => $value) {
            $tab[] = $value;
        }
        $id = $tableauDonneesFormulaire["id"] ?? null;
        $trajet = new Trajet($id, $tab[3], $tab[4], new DateTime($tab[5]), $tab[6], (new UtilisateurRepository())->recupererParClePrimaire($tab[7]), isset($_REQUEST["nonFumeur"]));

        return $trajet;
    }


}

?>