<h1>Liste des utilisateurs</h1>
<ul>
    <?php

    use App\Covoiturage\Modele\DataObject\Utilisateur;
    use App\Covoiturage\Lib\ConnexionUtilisateur;

    /** @var Utilisateur[] $utilisateurs */

    foreach ($utilisateurs as $utilisateur):
        $loginHTML = htmlspecialchars($utilisateur->getLogin());
        $loginURL = rawurlencode($utilisateur->getLogin());
        echo '
        <li><p> Utilisateur de login <a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . $loginURL . '">' .
            $loginHTML . '</a>';
        if (ConnexionUtilisateur::estAdministrateur()) {
            echo ' (<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginURL . '">Modifier ?</a>, 
            <a href="controleurFrontal.php?controleur=utilisateur&action=supprimer&login=' . $loginURL . '">Supprimer ?</a>)';
        }

        echo '</p></li>';
    endforeach; ?>
</ul>
