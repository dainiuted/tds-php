<?php

use App\Covoiturage\Configuration\ConfigurationSite;
?>
<form method="<?php echo ConfigurationSite::getDebug() ? 'GET' : 'POST'; ?>" action="controleurFrontal.php">
    <input type='hidden' name='action' value='connecter'>
    <input type='hidden' name='controleur' value='utilisateur'>
    <fieldset>
        <legend>Formulaire connexion :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="login" id="login_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdpL_id">Mot de passe &#42;</label>
            <input class="InputAddOn-field" type="password" placeholder="•••••••••" name="mdpL" id="mdpL_id" required>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>