<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;

class UtilisateurRepository extends AbstractRepository
{


    public function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        return new Utilisateur($utilisateurFormatTableau[0], $utilisateurFormatTableau[1], $utilisateurFormatTableau[2], $utilisateurFormatTableau[3], $utilisateurFormatTableau[4], $utilisateurFormatTableau[5], $utilisateurFormatTableau[6], $utilisateurFormatTableau[7]);

    }


    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom", "mdpHache", "estAdmin", "email", "emailAValider", "nonce"];
    }


    /**
     * @return Trajet[]
     */
    public static function recupererTrajetsCommePassager(Utilisateur $utilisateur): array
    {

        $sql = "SELECT * FROM Trajets t
                JOIN Passager p ON p.trajetId=t.id
                WHERE passagerLogin = :login";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array("login" => $utilisateur->getLogin());

        $pdoStatement->execute($values);
        $listeTrajet = $pdoStatement->fetch();

        return $listeTrajet;


    }


    protected function getNomTable(): string
    {
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string
    {
        return "login";
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
            "mdpHacheTag" => $utilisateur->getMdpHache(),
            "estAdminTag" => $utilisateur->getEstAdmin() ? 1 : 0,
            "emailTag" => $utilisateur->getEmail(),
            "emailAValiderTag" => $utilisateur->getEmailAValider(),
            "nonceTag" => $utilisateur->getNonce(),
        );
    }
}

